df_learning_train = df_learning %>% 
  filter(set == "train") %>% 
  filter(!is.na(Value))

df_learning_tmp  = df_learning %>% 
  mutate(target_tmp = as.integer(set == "test"))

df_learning_tmp = df_learning_tmp %>% 
  sample_frac(1)


  

NFOLD = 10
df_learning_tmp = df_learning_tmp %>% 
  mutate(folders = row_number() %% NFOLD)

df_train = df_learning_tmp %>% 
  filter(folders != max(folders)) %>% 
  select(-folders)

df_valid = df_learning_tmp %>% 
  filter(folders == max(folders)) %>% 
  select(-folders)

set.seed(1)
df_train = df_train %>%
  sample_n(500000)

# df_test = df_learning %>%
#   filter(set == "test")

# df_test %>% 
#   group_by(ForecastId, Sampling) %>% 
#   summarise(forecast_windows = n()) %>%
#   ungroup() %>% 
#   group_by(forecast_windows, Sampling) %>% 
#   summarise(n_ForecastId = n())

# print("Modeling")
to_drop = c("SiteId", "ForecastId", "Timestamp_weather", "Timestamp", "Timestamp_lead", 
            "set", "weight", "Value_lead", "lead")

categorical_feature = c()

target = "target_tmp"

X_train = df_train %>%
  select(-one_of(c(to_drop, target))) %>%
  as.matrix()

X_valid = df_valid %>%
  select(-one_of(c(to_drop, target))) %>%
  as.matrix()

y_train = df_train %>% pull(target)
y_valid = df_valid %>% pull(target)

# weight_train = df_train %>% pull(perishable) * 0.25 + 1
# weight_valid = df_valid %>% pull(perishable) * 0.25 + 1

params <- list(num_leaves = 2^5-2
               ,objective = "binary"
               # ,max_depth = 8
               # ,min_data_in_leaf = 200
               ,learning_rate = 0.3
               ,metric = "auc")

lgb_train = lgb.Dataset(X_train, label = y_train) #, weight = weight_train)
lgb_valid = lgb.Dataset(X_valid, label = y_valid) #, weight = weight_valid)
# lgb_test = lgb.Dataset(X_test) # predict is done on X_test with lightgbm

system.time(
  model_lgb <- lgb.train(params, lgb_train, nrounds = 100000,
                         categorical_feature = categorical_feature,
                         valids = list(train = lgb_train, valid=lgb_valid),
                         early_stopping_rounds = 50, verbose = 1, eval_freq = 10)
) %>% print()

importance_matrix = lgb.importance(model = model_lgb)
print(importance_matrix[])


df_learning_tmp %>% 
  sample_n(50000) %>% 
  drop_constant_col() %>% 
  get_graph_vistree(cols_to_drop = to_drop, target = target)

