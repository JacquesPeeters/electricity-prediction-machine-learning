Sys.setenv(TZ="Europe/Paris")

library(lightgbm)
library(data.table)
library(RcppRoll)
library(dtplyr)
library(stringr)
library(tidyverse)
library(lubridate)

source("src/utilities.R")

###
# Read data ---------------------------------------------------------------
print("Read data")

holidays = read_csv("./data/holidays.csv") %>% select(-1)
metadata = read_csv("./data/metadata.csv")
submission_format = read_csv("./data/submission_format.csv")
submission_frequency = read_csv("./data/submission_frequency.csv")
train = read_csv("./data/train.csv")
weather = read_csv("./data/weather.csv") %>% select(-1)

test = submission_format %>% 
  mutate(Value = NA)

###
# Feature engineering -----------------------------------------------------

# Clean outliers in train data --------------------------------------------
print("Clean outliers in train data")
k = 10
train = train %>%
  group_by(ForecastId) %>%
  mutate(value_mean = mean(Value, na.rm = T)) %>% 
  ungroup() %>%
  mutate(Value = if_else((Value / value_mean) > k, # | (Value / value_roll_median) < 1/k,
                         true = as.double(NA), false = Value)) %>% 
  select(-value_mean)

# forecast_info - information about ForecastId ----------------------------
print("forecast_info - information about ForecastId")
forecast_info = test %>% 
  group_by(ForecastId) %>%
  mutate(timestep_minutes = (Timestamp - lag(Timestamp)) / dseconds(60)) %>% 
  summarise(size = n(), timestep_minutes = max(timestep_minutes, na.rm = T)) %>% 
  ungroup()

forecast_info = forecast_info %>% 
  mutate(step_day = 1440 / timestep_minutes,
         step_hour = 60 / timestep_minutes) %>% 
  mutate(step_hour = ifelse(step_hour < 1, NA, step_hour) %>% as.integer())

# df_target - Generate target values - Y ----------------------------------------------
print("df_target - Generate target values - Y")

target = train %>% 
  select(SiteId, ForecastId, Timestamp, Value) %>% 
  tbl_dt() %>% 
  group_by(ForecastId) %>% 
  filter(sum(Value, na.rm = T) > 0) %>% 
  ungroup()

# Don't process empty training sets in generate_target_loop
forecast_info = forecast_info %>%
  inner_join(target %>% distinct(ForecastId), by = "ForecastId")

generate_target = function(target, site_id, WINDOW_SIZE){
  # print(f_id)
  target_tmp = target[SiteId == site_id]
  
  WINDOW_SIZE = seq_len(WINDOW_SIZE)
  
  target_tmp = target_tmp %>% 
    .[, shift(.SD, WINDOW_SIZE, NA, "lead", TRUE), 
      .SDcols=c("Timestamp", "Value"),
      by = c("ForecastId")] %>% 
    bind_cols(target_tmp %>% 
                select(-Value), .) 
  
  colA = paste("Timestamp_lead_", WINDOW_SIZE, sep = "")
  colB = paste("Value_lead_", WINDOW_SIZE, sep = "")
  target_tmp = melt.data.table(target_tmp, measure.vars = list(colA, colB), 
                               value.name = c("Timestamp_lead", "Value_lead"), variable.name = "lead",
                               variable.factor = F)
  
  target_tmp = target_tmp[ , lead := as.integer(lead)]
  
  # Discard asap useless lines
  target_tmp = target_tmp[!is.na(Value_lead)]
  
  # Try to subsample smartly, given metric
  n_obs = 200
  target_tmp = target_tmp[ , weight :=  (3 * n_obs - (2 * lead) + 1) / (2 * (n_obs ** 2))]
  
  # SAMPLING_SIZE = min(max(WINDOW_SIZE) * 10, nrow(target_tmp))
  # set.seed(1)
  # target_tmp = target_tmp[sample(.N,SAMPLING_SIZE, prob = weight, replace = T)] # Bottleneck when replace = F
  
  return(target_tmp)
}

generate_target_loop = function(target, site_id, WINDOW_SIZE){
  # Avoid memory overload
  # res = vector("list", length = length(site_id))
  for(i in seq_along(site_id)){
    file = generate_target(target, site_id[i], WINDOW_SIZE[i])
    saveRDS(file, "./output_target/df_target_.rds")
  }
  res = bind_rows(res) # Outside of loop for efficiency
}

system.time(df_target <- generate_target_loop(target, forecast_info$SiteId, forecast_info$size))
rm(target)

df_target_test = generate_target_test(train, submission_format)

df_target = df_target %>% mutate(set = "train") %>% 
  bind_rows(df_target_test %>% mutate(set = "test"), .)
rm(df_target_test)